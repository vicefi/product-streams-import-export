<?php
/**
 * (c) Victor Efimov <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\SchemaTool;
use DoctrineExtensions\Query\Mysql\GroupConcat;
use Shopware\Components\CacheManager;
use Shopware\Components\Model\ModelManager;
use Shopware\CustomModels\ImportExport\Expression;
use Shopware\CustomModels\ImportExport\Logger as LoggerModel;
use Shopware\CustomModels\ImportExport\Profile;
use Shopware\CustomModels\ImportExport\Session;

# TODO use it in further versions
#use Shopware\Commands\EvvImportExport\ExportCommand;
#use Shopware\Commands\EvvImportExport\ImportCommand;
#use Shopware\Commands\EvvImportExport\ProfilesCommand;
#
#use Shopware\Components\EvvImportExport\Factories\DataFactory;
#use Shopware\Components\EvvImportExport\Factories\DataTransformerFactory;
#use Shopware\Components\EvvImportExport\Factories\FileIOFactory;
#use Shopware\Components\EvvImportExport\Factories\ProfileFactory;
#use Shopware\Components\EvvImportExport\FileIO\CsvFileReader;
#use Shopware\Components\EvvImportExport\FileIO\CsvFileWriter;
#use Shopware\Components\EvvImportExport\Logger\Logger;
#use Shopware\Components\EvvImportExport\Service\AutoImportService;
#use Shopware\Components\EvvImportExport\Service\AutoImportServiceInterface;
#use Shopware\Components\EvvImportExport\Service\ExportService;
#use Shopware\Components\EvvImportExport\Service\ImportService;
#use Shopware\Components\EvvImportExport\Service\ProfileService;
#use Shopware\Components\EvvImportExport\Service\UnderscoreToCamelCaseService;
#
#use Shopware\Components\EvvImportExport\UploadPathProvider;
#use Shopware\Components\EvvImportExport\Utils\FileHelper;
#
#use Shopware\Setup\EvvImportExport\Exception\MinVersionException;
#use Shopware\Setup\EvvImportExport\Install\DefaultProfileInstaller;
#use Shopware\Setup\EvvImportExport\Install\InstallerInterface;
#use Shopware\Setup\EvvImportExport\Install\MainMenuItemInstaller;
#use Shopware\Setup\EvvImportExport\Install\OldAdvancedMenuInstaller;
#use Shopware\Setup\EvvImportExport\SetupContext;
#use Shopware\Setup\EvvImportExport\Update\DefaultProfileUpdater;
#use Shopware\Setup\EvvImportExport\Update\Update01MainMenuItem;
#use Shopware\Setup\EvvImportExport\Update\Update02RemoveForeignKeyConstraint;
#use Shopware\Setup\EvvImportExport\Update\Update03DefaultProfileSupport;
#use Shopware\Setup\EvvImportExport\Update\Update04CreateColumns;
#use Shopware\Setup\EvvImportExport\Update\Update05CreateCustomerCompleteProfile;
#use Shopware\Setup\EvvImportExport\Update\Update06CreateCategoryTranslationProfile;
#use Shopware\Setup\EvvImportExport\Update\UpdaterInterface;

/**
 * Shopware SwagImportExport Plugin - Bootstrap
 *
 * @category  Shopware
 *
 * @copyright  Copyright (c) Victor Efimov (http://localhost)
 */
final class Shopware_Plugins_Backend_EvvImportExport_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    /**
     * @var DataFactory
     */
    private $dataFactory;

    /**
     * @var ProfileFactory
     */
    private $profileFactory;

    /**
     * @var FileIOFactory
     */
    private $fileIOFactory;

    /**
     * @var DataTransformerFactory
     */
    private $dataTransformerFactory;

    /**
     * @var Enlight_Components_Db_Adapter_Pdo_Mysql
     */
    private $db;

    /**
     * @var ModelManager
     */
    private $em;

    /**
     * Returns the plugin label which is displayed in the plugin information and
     * in the Plugin Manager.
     *
     * @return string
     */
    public function getLabel()
    {
        return 'Shopware Product Stream Import/Export';
    }

    /**
     * Returns the version of the plugin as a string
     *
     * @throws RuntimeException
     *
     * @return string
     */
    public function getVersion()
    {
        $info = json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'plugin.json'), true);

        if ($info) {
            return $info['currentVersion'];
        }
        throw new RuntimeException('The plugin has an invalid version file.');
    }

    /**
     * After init event of the bootstrap class.
     *
     * The afterInit function registers the custom plugin models.
     */
    public function afterInit()
    {
        $this->db = $this->get('db');
        $this->em = $this->get('models');

        $this->registerCustomModels();
        $this->registerMyNamespace();
    }

    /**
     * {@inheritdoc}
     */
    public function getCapabilities()
    {
        return [
            'install' => true,
            'update' => true,
            'enable' => true,
            'secureUninstall' => true,
        ];
    }

    /**
     * Install function of the plugin bootstrap.
     *
     * Registers all necessary components and dependencies.
     *
     * @throws Exception
     *
     * @return bool
     */
    public function install()
    {
        if (!$this->assertMinimumVersion('5.4.0')) {
            throw new MinVersionException('This plugin requires Shopware 5.4.0 or a later version');
        }
        /** @var CacheManager $cacheManager */
        $cacheManager = $this->get('shopware.cache_manager');
        $cacheManager->clearProxyCache();

        /*$setupContext = new SetupContext(
            $this->get('config')->get('version'),
            $this->getVersion(),
            SetupContext::NO_PREVIOUS_VERSION
        );*/

        /*$installers = [];
        $installers[] = new DefaultProfileInstaller($setupContext, $this->get('dbal_connection'));
        $installers[] = new MainMenuItemInstaller($setupContext, $this->get('models'));
        $installers[] = new OldAdvancedMenuInstaller($setupContext, $this->get('models'));*/

        # TODO use it later
        #$this->createDatabase();
        $this->registerControllers();
        $this->createDirectories();
        $this->createConfiguration();

        /** @var InstallerInterface $installer */
        /*foreach ($installers as $installer) {
            if (!$installer->isCompatible()) {
                continue;
            }
            $installer->install();
        }*/

        return true;
    }


    /**
     * @param string $oldVersion
     *
     * @throws MinVersionException
     *
     * @return array
     */
    public function update($oldVersion)
    {
        // do nothing yet
    }

    /**
     * Uninstall function of the plugin.
     * Fired from the plugin manager.
     *
     * @return array
     */
    public function uninstall()
    {
        $this->secureUninstall();

        $this->removeDatabaseTables();

        return [
            'success' => true,
            'invalidateCache' => $this->getInvalidateCacheArray(),
        ];
    }

    /**
     * @return array
     */
    public function secureUninstall()
    {
        return [
            'success' => true,
            'invalidateCache' => $this->getInvalidateCacheArray(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function enable()
    {
        return [
            'success' => true,
            'invalidateCache' => $this->getInvalidateCacheArray(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function disable()
    {
        return [
            'success' => true,
            'invalidateCache' => $this->getInvalidateCacheArray(),
        ];
    }

    /**
     * Register components directory
     */
    public function registerMyNamespace()
    {
        // Register Doctrine RegExp extension
        /** @var Configuration $config */
        $config = $this->em->getConfiguration();
        $classLoader = new \Doctrine\Common\ClassLoader('DoctrineExtensions', $this->Path() . 'Components/');
        $classLoader->register();
        $config->addCustomStringFunction('GroupConcat', GroupConcat::class);

        $this->get('loader')->registerNamespace('Shopware\Components', $this->Path() . 'Components/');
        $this->get('loader')->registerNamespace('Shopware\Commands', $this->Path() . 'Commands/');
        $this->get('loader')->registerNamespace('Shopware\Subscriber', $this->Path() . 'Subscriber/');
        $this->get('loader')->registerNamespace('Shopware\Setup', $this->Path() . 'Setup/');
    }

    /**
     * @return DataFactory
     */
    public function getDataFactory()
    {
        if ($this->dataFactory === null) {
            $this->dataFactory = Enlight_Class::Instance(DataFactory::class);
        }

        return $this->dataFactory;
    }

    /**
     * @return ProfileFactory
     */
    public function getProfileFactory()
    {
        if ($this->profileFactory === null) {
            $this->profileFactory = Enlight_Class::Instance(ProfileFactory::class);
        }

        return $this->profileFactory;
    }

    /**
     * @return FileIOFactory
     */
    public function getFileIOFactory()
    {
        if ($this->fileIOFactory === null) {
            $this->fileIOFactory = Enlight_Class::Instance(FileIOFactory::class);
        }

        return $this->fileIOFactory;
    }

    /**
     * @return DataTransformerFactory
     */
    public function getDataTransformerFactory()
    {
        if ($this->dataTransformerFactory === null) {
            $this->dataTransformerFactory = Enlight_Class::Instance(DataTransformerFactory::class);
        }

        return $this->dataTransformerFactory;
    }

    /**
     * @param string $version
     *
     * @return bool
     */
    public function checkMinVersion($version)
    {
        return $this->assertMinimumVersion($version);
    }

    /**
     * This event needs to be registered so it's guaranteed this plugin's afterInit() method will always be called and
     * gets a chance to register the plugin's namespaces.
     *
     * @param \Enlight_Event_EventArgs $args
     */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        // Nothing to do
    }

    /**
     * @return UploadPathProvider
     */
    public function registerUploadPathProvider()
    {
        return new UploadPathProvider(Shopware()->DocPath());
    }

    /**
     * @return Logger
     */
    public function registerLogger()
    {
        return new Logger(
            $this->get('evv_import_export.csv_file_writer'),
            $this->get('models'),
            $this->Application()->Container()->getParameter('shopware.app.rootdir') . 'var/log'
        );
    }

    /**
     * @return ImportService
     */
    public function registerImportService()
    {
        return new ImportService(
            $this->getProfileFactory(),
            $this->getFileIOFactory(),
            $this->getDataFactory(),
            $this->getDataTransformerFactory(),
            $this->get('evv_import_export.logger'),
            $this->get('evv_import_export.upload_path_provider'),
            Shopware()->Auth(),
            $this->get('shopware_media.media_service')
        );
    }

    /**
     * @return ExportService
     */
    public function registerExportService()
    {
        return new ExportService(
            $this->getProfileFactory(),
            $this->getFileIOFactory(),
            $this->getDataFactory(),
            $this->getDataTransformerFactory(),
            $this->get('evv_import_export.logger'),
            $this->get('evv_import_export.upload_path_provider'),
            Shopware()->Auth(),
            $this->get('shopware_media.media_service')
        );
    }

    /**
     * @return ProfileService
     */
    public function registerProfileService()
    {
        return new ProfileService(
            $this->get('models'),
            new Symfony\Component\Filesystem\Filesystem(),
            $this->get('snippets')
        );
    }

    /**
     * @return CsvFileReader
     */
    public function registerCsvFileReader()
    {
        return new CsvFileReader(
            $this->get('evv_import_export.upload_path_provider')
        );
    }

    /**
     * @return CsvFileWriter
     */
    public function registerCsvFileWriter()
    {
        return new CsvFileWriter(
            new FileHelper()
        );
    }

    /**
     * Injects Ace Editor used in Conversions GUI
     *
     * @param Enlight_Event_EventArgs $args
     */
    public function injectBackendAceEditor(Enlight_Event_EventArgs $args)
    {
        /** @var Shopware_Controllers_Backend_Index $controller */
        $controller = $args->get('subject');
        $request = $controller->Request();
        $response = $controller->Response();
        $view = $controller->View();

        if (!$request->isDispatched() || $response->isException() || !$view->hasTemplate()) {
            return;
        }

        $view->addTemplateDir($this->Path() . 'Views/');
        $view->extendsTemplate('backend/evv_import_export/menu_entry.tpl');
    }

    /**
     * Adds the console commands (sw:import and sw:export)
     *
     * @return ArrayCollection
     */
    public function onAddConsoleCommand()
    {
        return new ArrayCollection(
            [
                new ImportCommand(),
                new ExportCommand(),
                new ProfilesCommand(),
            ]
        );
    }

    /**
     * Create plugin configuration
     */
    public function createConfiguration()
    {
        $form = $this->Form();

        $form->setElement(
            'combo',
            'EvvImportExportErrorMode',
            [
                'label' => 'Continue import/export if an error occurs during the process',
                'store' => [
                    [false, ['de_DE' => 'Nein', 'en_GB' => 'No']],
                    [true, ['de_DE' => 'Ja', 'en_GB' => 'Yes']],
                ],
                'required' => false,
                'multiSelect' => false,
                'value' => false,
            ]
        );

        $form->setElement(
            'file',
            'file-name',
            [
                'label' => 'File name',
                'value' => '',
            ]
        );

        $form->setElement(
            'number',
            'batch-size-export',
            [
                'label' => 'Batch-Size Export',
                'value' => 1000,
                'minValue' => 0,
            ]
        );

        $form->setElement(
            'number',
            'batch-size-import',
            [
                'label' => 'Batch-Size Import',
                'value' => 50,
                'minValue' => 0,
            ]
        );

        $form->setElement(
            'combo',
            'EvvImportExportImageMode',
            [
                'label' => 'Image import mode',
                'store' => [
                    [1, ['de_DE' => 'Gleiche Artikelbilder erneut verwenden', 'en_GB' => 'Re-use same article images']],
                    [2, ['de_DE' => 'Gleiche Artikelbilder nicht erneut verwenden', 'en_GB' => 'Don\'t re-use article images']],
                ],
                'required' => false,
                'multiSelect' => false,
                'value' => 2,
            ]
        );

        $form->setElement(
            'checkbox',
            'useCommaDecimal',
            [
                'label' => 'Use comma as decimal separator',
                'value' => true,
            ]
        );

        $this->createTranslations();
    }

    /**
     * Translation for plugin configuration
     */
    public function createTranslations()
    {
        $translations = include_once __DIR__ . '/Resources/Translations/config.php';

        if ($this->assertMinimumVersion('4.2.2')) {
            $this->addFormTranslations($translations);
        }
    }

    /**
     * @return UnderscoreToCamelCaseService
     */
    public function registerUnderscoreToCamelCaseService()
    {
        return new UnderscoreToCamelCaseService();
    }

    /**
     * Registers all necessary events.
     */
    protected function registerEvents()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Front_StartDispatch',
            'onStartDispatch'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.csv_file_writer',
            'registerCsvFileWriter'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.csv_file_reader',
            'registerCsvFileReader'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.logger',
            'registerLogger'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.import_service',
            'registerImportService'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.export_service',
            'registerExportService'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.profile_service',
            'registerProfileService'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.upload_path_provider',
            'registerUploadPathProvider'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.auto_importer',
            'registerAutoImportService'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_evv_import_export.underscore_camelcase_service',
            'registerUnderscoreToCamelCaseService'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Backend_Index',
            'injectBackendAceEditor'
        );

        $this->subscribeEvent(
            'Shopware_Console_Add_Command',
            'onAddConsoleCommand'
        );

        $this->subscribeEvent(
            'Shopware_CronJob_CronAutoImport',
            'onCronImport'
        );
    }

    protected function registerControllers()
    {
        $backendControllers = [
            'EvvImportExport',
            'EvvImportExportImport',
            'EvvImportExportExport',
            'EvvImportExportProfile',
            'EvvImportExportConversion',
            'EvvImportExportSession',
            'EvvImportExportCron',
        ];

        foreach ($backendControllers as $ctrl) {
            $this->registerController('Backend', $ctrl);
        }

        $this->registerController('Frontend', 'EvvImportExport');
    }

    private function createDirectories()
    {
        $importCronPath = Shopware()->DocPath() . 'files/import_cron/product_streams/';
        if (!file_exists($importCronPath)) {
            mkdir($importCronPath, 0777, true);
        }

        if (!file_exists($importCronPath . '.htaccess')) {
            copy($this->Path() . 'Setup/EvvImportExport/template', $importCronPath . '/.htaccess');
        }

        $importExportPath = Shopware()->DocPath() . 'files/import_export/product_streams/';
        if (!file_exists($importExportPath)) {
            mkdir($importExportPath, 0777, true);
        }

        if (!file_exists($importExportPath . '.htaccess')) {
            copy($this->Path() . 'Setup/EvvImportExport/template', $importExportPath . '/.htaccess');
        }
    }

    /**
     * Creates the plugin database table over the doctrine schema tool.
     */
    private function createDatabase()
    {
        $schemaTool = new SchemaTool($this->em);
        $doctrineModels = $this->getDoctrineModels();

        $tableNames = $this->removeTablePrefix($schemaTool, $doctrineModels);

        /** @var ModelManager $modelManger */
        $modelManger = $this->get('models');
        $schemaManager = $modelManger->getConnection()->getSchemaManager();
        if (!$schemaManager->tablesExist($tableNames)) {
            $schemaTool->createSchema($doctrineModels);
        }
    }

    private function updateDatabase()
    {
        $schemaTool = new SchemaTool($this->em);
        $doctrineModels = $this->getDoctrineModels();
        $schemaTool->updateSchema($doctrineModels, true);
    }

    /**
     * Removes the plugin database tables
     */
    private function removeDatabaseTables()
    {
        $tool = new SchemaTool($this->em);
        $classes = $this->getDoctrineModels();
        $tool->dropSchema($classes);
    }

    /**
     * Helper method to return all the caches, that need to be cleared after
     * updating / uninstalling / enabling / disabling a plugin
     *
     * @return array
     */
    private function getInvalidateCacheArray()
    {
        return ['config', 'backend', 'proxy'];
    }

    /**
     * @return array
     */
    private function getDoctrineModels()
    {
        return [
            $this->em->getClassMetadata(Session::class),
            $this->em->getClassMetadata(LoggerModel::class),
            $this->em->getClassMetadata(Profile::class),
            $this->em->getClassMetadata(Expression::class),
        ];
    }

    /**
     * @param SchemaTool $tool
     * @param array      $classes
     *
     * @return array
     */
    private function removeTablePrefix(SchemaTool $tool, array $classes)
    {
        $schema = $tool->getSchemaFromMetadata($classes);
        $tableNames = [];
        foreach ($schema->getTableNames() as $tableName) {
            $explodedTableNames = explode('.', $tableName);
            $tableNames[] = array_pop($explodedTableNames);
        }

        return $tableNames;
    }

    /**
     * Rename duplicate profile names to prevent integrity constraint mysql exceptions.
     */
    private function renameDuplicateProfileNames()
    {
        $connection = $this->get('dbal_connection');
        $profiles = $connection->fetchAll('SELECT COUNT(id) as count, name FROM s_import_export_profile GROUP BY name');

        foreach ($profiles as $profile) {
            if ((int) $profile['count'] === 1) {
                continue;
            }

            $profilesWithSameName = $connection->fetchAll(
                'SELECT * FROM s_import_export_profile WHERE name=:name ORDER BY id',
                ['name' => $profile['name']]
            );

            $this->addSuffixToProfileNames($profilesWithSameName);
        }
    }

    /**
     * @param array $profiles
     */
    private function addSuffixToProfileNames($profiles)
    {
        $dbalConnection = $this->get('dbal_connection');

        foreach ($profiles as $index => $profile) {
            if ($index === 0) {
                continue;
            }

            $dbalConnection->executeQuery(
                'UPDATE s_import_export_profile SET name = :name WHERE id=:id',
                ['name' => uniqid($profile['name'] . '_', true), 'id' => $profile['id']]
            );
        }
    }
}

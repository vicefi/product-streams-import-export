<?php

return [
    'en_GB' => [
        'SwagImportExportImageMode' => [
            'label' => 'Image import mode',
        ],
        'SwagImportExportErrorMode' => [
            'label' => 'Continue Product Stream import/export if an error occurs during the process',
        ],
        'useCommaDecimal' => [
            'label' => 'Use comma as decimal separator',
        ],
    ],
    'de_DE' => [
        'SwagImportExportImageMode' => [
            'label' => 'Bildimport-Modus',
        ],
        'SwagImportExportErrorMode' => [
            'label' => 'Mit Product Stream Import/Export fortfahren, wenn ein Fehler auftritt.',
        ],
        'useCommaDecimal' => [
            'label' => 'Komma als Dezimal-Trennzeichen nutzen',
        ],
    ],
];